For this guide you will require nodejs (https://nodejs.org/en/). It also assumes that you run Windows, but you should be able to figure it out on other systems as well.

Step 1: Download the ZIP file.

Step 2: Extract Contents of zip file.

Step 3: Open a Node.js Command Prompt.

Step 4: Do `cd C:\path\to\folder`

Step 5: Run `node .` in command prompt and boom your done!

(Dont close command prompt or the status will stop)
